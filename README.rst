-----------------------------------------------------------------------
                          Context Mirror Bot
-----------------------------------------------------------------------

Program for creating and maintaining a mirror for Context_, intended to
be run as a cron job but also handy when checking for updates to
Context manually.

-----------------------------------------------------------------------
                              Background
-----------------------------------------------------------------------

With each release of the *current* and *beta* branches, a new zipball
containing the latest code is uploaded to the Pragma ADE servers and
announced on the `official website`_. Since the tree is no under SCM
nor browsable online, different workarounds have been put into place by
the community. The `first mirror`_ provided a reliable git repository
with the most recent code for many years. It was discontinued_ in early
2014. By that time, a similar mirror was installed on the
`Context Garden`_, but apparently it prevents cloning the repository.
Also, it’s a good thing in general to have some redundancy in case the
alternatives go down.

-----------------------------------------------------------------------
                             How It Works
-----------------------------------------------------------------------

The *Context Mirror Bot* scrapes abovementioned download pages for
timestamps of the most recent zips and matches them against the state
of a repository. When it detects a new zipball, the bot downloads and
extracts it into the local repo, committing the new state along with
its timestamp. Contrary to previous mirrors, this one keeps the *beta*
and *current* in separate branches, producing higher quality diffs.

-----------------------------------------------------------------------
                                How-to
-----------------------------------------------------------------------
Install
*******

To build the executable you need Ocaml_ (tested with version 4.01
through 4.05) as well as Omake_. *Context Mirror Bot* requires the
``fileutils``. ``ocurl``, ``syslog``, ``uri``, and ``xmlm`` libraries.
They should be readily available through Opam_.

Note: The *syslog* package is currently unavailable for recent compiler
versions, preventing the use of the mirror bot with Ocaml from 4.06
upwards.

If the dependencies are in place, simply issue ``omake`` in the
repository directory and the executable ``context_mirror_bot`` is
created.

Prerequisites
*************

The *Context Mirror Bot* assumes an ``unzip(1)`` executable accessible
from the ``$PATH``. The same goes for standard userland utilities like
``cp(1)`` and ``rm(1)``. In dubious cases, the `GNU Coreutils`_ are
assumed the canonical implementation.

It goes without saying that ``git(1)`` must be installed on the system
as well.

Run
***

To familiarize yourself with the options, run ::

    context_mirror_bot --help

Currently this presents you with this list of options:

--check         check source timestamps
--status        print status of local repo
--sync          sync local repo and remotes
--update        update from sources
--prepare       prepare local repository
--download      download source packages
--clean         remove leftover files
--push          push to mirror
--files         install config and wrapper files
--verbose       enable verbose output

To simply retrieve the current state of Context, choose ``--download``.
The destination directory is currently hardcoded in the variable
``repo_root`` in the program header -- adapt it to your needs. As long
as the target location is writable, the *Mirror Bot* will not leave it.
Run ``context_mirror_bot --clean`` to remove unnecessary temporary
files from the directory.

In order to set up a mirror you need an SSH key. Store the private key
in the file ``context_mirror_bot_id_rsa`` inside the current working
directory and the *Mirror Bot*. The public key must be registered with
an online repository on a source code hoster like Bitbucket or Github..

Now edit the ``gitconfig`` variable in the program header to include
the name of the user you intend the *Mirror Bot* to run under. Also,
edit the variable ``mirror_repo`` to point to the destination
repository at the code hoster. Now recompile with ``omake`` and run
``context_mirror_bot --update`` -- the bot will retrieve the latest
sources and commit them on top of the latest state of the earlier
repository on Gitorious. The result is then pushed to the remote
server.

-----------------------------------------------------------------------
                               Locations
-----------------------------------------------------------------------

The author’s mirror resides in its own `repo at Gitlab`_.

-----------------------------------------------------------------------
                                 About
-----------------------------------------------------------------------

Written by `Philipp Gesang`_. Inspired by the `original script` by
Marius, the long-term maintainer of the third-party mirror.

The *Context Mirror Bot* is released under the terms of the GNU General
Public License, version 3 (no later or earlier). See the file
``COPYING`` for the full license text.

Context_ is supplied under the terms of the GNU General Public License,
version 2 (no later or earlier). See the file ``context-license`` for
details. This file will also be located in the mirror repository tree
under the name ``COPYING``.

.. _Context:            http://www.pragma-ade.com/
.. _official website:   http://www.pragma-ade.com/download-1.htm
.. _first mirror:       https://gitorious.org/context
.. _discontinued:       http://www.ntg.nl/pipermail/ntg-context/2014/077380.html
.. _Context Garden:     http://git.contextgarden.net/context/context
.. _repo at Gitlab:     https://gitlab.com/phgsng/context-mirror
.. _dedicated user:     https://t.b.d/context-mirror-bot
.. _Ocaml:              http://ocaml.org/
.. _Opam:               http://opam.ocamlpro.com/
.. _Omake:              http://omake.metaprl.org/index.html
.. _Philipp Gesang:     mailto:philipp.gesang@alumni.uni-heidelberg.de
.. _original script:    https://gitorious.org/context/context/source/d493b9c93ad826044bd17889da2ee099c8ed72d5:context_git_update.pl
.. _GNU Coreutils:      http://www.gnu.org/s/coreutils/

